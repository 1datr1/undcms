<?php 
$_SUCC_MESS='������ ������� ��������.';
$_REDIR_TIMEOUT=0;
$_ACTION_STOPPED=false;
// execute action
function inc_action($act,$json=false)
{
	global $_BASE_PATH,$_ACTION_STOPPED,$_DBS,$_DB,$_SITE,$_MODULES,$_PAGE,$_QUERY,$_EP,$_ACT_ERROR,$_SUCC_MESS,$_REDIR_TIMEOUT;
	$_SESSION['actmess']=Array();
	$_SESSION['q_last_post']=$_POST;
	
	$_module='';
	$tail='';
	
	if(get_module($act,$_module,$tail))
	{
	//echo "| $ >> >>";
		
		if(!empty($_MODULES[$_module]) || in_array($_module,$_MODULES))
		{
			//echo ">> $page ? $_module : $tail>>";
			$_FILEPATH_HEAD=$_BASE_PATH."/modules/$_module/ep/$_EP"; // ���� � ����� ������ � ������
			$act=$tail;
				
		}
		else
			$_FILEPATH_HEAD=$_BASE_PATH."/sites/$_SITE/ep/$_EP"; // ���� � ����� ������
	}
	else
	{
		$_FILEPATH_HEAD=$_BASE_PATH."/sites/$_SITE/ep/$_EP"; // ���� � ����� ������
	}
	
	if($json)
	{
		ob_start();

		if(folder_exists("$_FILEPATH_HEAD/actions/$act"))
		{
			include "$_FILEPATH_HEAD/actions/$act/index.php";
		}
		else
		{
			include "$_FILEPATH_HEAD/actions/$act.php";
		}
		$thebody = ob_get_contents();
		ob_end_clean();

		//return 
		echo json_encode(
				Array(
						'body'=>$thebody,
						'messages'=>$_TITLE,
						'redirect'=>$_JS,						
				)
		);
	}
	else
	{
	/*	ob_start();*/


		if(folder_exists("$_FILEPATH_HEAD/actions/$act"))
		{
			include "$_FILEPATH_HEAD/actions/$act/index.php";
		}
		else
		{
			include "$_FILEPATH_HEAD/actions/$act.php";
		}
	/*	$thebody = ob_get_contents();
		ob_end_clean();*/
	//	return $thebody;
	}
	echo ">> $_ACTION_STOPPED >>";
	if(!$_ACTION_STOPPED)
	{
		if($_ACT_ERROR)	// �������� ����������� � �������
		{
			redirect($_POST['urlfrom']);
		}
		else 
		{
			global $_REDIR_TIMEOUT;
			unset($_SESSION['q_last_post']);
			if($_REDIR_TIMEOUT)
			{		
				echo get_page('redirect',false,Array(
						'URL'=>$_POST['urlto'],
						'MESS'=>$_SUCC_MESS,
						'TIMEOUT'=>$_REDIR_TIMEOUT,
						)
					);
			}
			else
				redirect($_POST['urlto']);
		}
	}
	exit();
}

function stop_action()
{
	global $_ACTION_STOPPED;
	$_ACTION_STOPPED=true;
}

function getref($ref)
{
	global $_PSEUDOFOLDER;
	if($_PSEUDOFOLDER)
		return $ref;
	else 
		return "index.php?q=$ref";
}

function last_post_val($varname)
{
	global $_BASE_PATH;
	if(empty($_SESSION['q_last_post'][$varname]))
		return '';
	return $_SESSION['q_last_post'][$varname];
}

function set_lpv($varname,$val)
{
	global $_BASE_PATH;
	if(empty($_SESSION['q_last_post']))
		$_SESSION['q_last_post']=Array();
	$_SESSION['q_last_post'][$varname]=$val;
}
// begin form
function form_begin($act,$opts=null,$redir_url=null)
{
	global $_BASE_PATH;
	xdefarray(Array('class'=>'','confirm'=>false),$opts);
	if($opts['confirm']!=false)
	{
		jqready_gather("
			\$('.".$opts['class']."').submit(function() {
		if(confirm('".$opts['confirm']."'))
				return true;
			return false;
		}
	);
							
");
	}
	$_class="";
	if($frmclass=='')
		$_class=" class=\"$frmclass\"";
	global $_URL_BASE;
	
	if($_URL_BASE[0]!='/')
		$_URL_BASE="/$_URL_BASE";
	if($_URL_BASE[strlen($_URL_BASE)-1]!='/')
		$_URL_BASE="$_URL_BASE/";
	
	echo "<form method=\"post\" class=\"form-horizontal ".$opts['class']."\" action=\"{$_URL_BASE}index.php?act=$act\">";
	echo "<input type=\"hidden\" name=\"urlfrom\" value=\"".$_SERVER['REQUEST_URI']."\" />";
	if($redir_url==null)
	{
		echo "<input type=\"hidden\" name=\"urlto\" value=\"".$_SERVER['REQUEST_URI']."\" />";
	}
	else 
	{
		echo "<input type=\"hidden\" name=\"urlto\" value=\"$redir_url\" />";
	}
}

function form_end()
{
	echo "</form>";
}
// get the form with name
function get_form($_form,$_PARAMS=null,$json=false)
{
	global $_BASE_PATH,$_SITE,$_DB;
	global $_PAGE;
	global $_QUERY;
	global $_EP;
	if($json)
	{
		ob_start();
	
	
		if(folder_exists($_BASE_PATH."/sites/$_SITE/ep/$_EP/forms/$_form"))
		{
			include $_BASE_PATH."/sites/$_SITE/ep/$_EP/forms/$_form/index.php";
		}
		else
		{
			include $_BASE_PATH."/sites/$_SITE/ep/$_EP/forms/$_form.php";
		}
		$thebody = ob_get_contents();
		ob_end_clean();
	
		return json_encode(
				Array(
						'body'=>$thebody,
						'title'=>$_TITLE,
						'js'=>$_JS,
						'css'=>$_CSS,
						'meta'=>$_META,
						'jsforms'=>$_SCRIPT_forms,
						'jsif'=>$_JSIF,
				)
		);
	}
	else
	{
		ob_start();
	
	
		if(folder_exists("$_BASE_PATH/sites/$_SITE/ep/$_EP/forms/$_form"))
		{
			include "$_BASE_PATH/sites/$_SITE/ep/$_EP/forms/$_form/index.php";
		}
		else
		{
		include "$_BASE_PATH/sites/$_SITE/ep/$_EP/forms/$_form.php";
		}
		$thebody = ob_get_contents();
		ob_end_clean();
		return $thebody;
	}
}

function error404()
{
	ob_end_clean();
	header("HTTP/1.0 404 Not Found");
	global $_BASE_PATH;
	include "$_BASE_PATH/errors/404.php";
	exit;
}

function error403()
{	
	ob_end_clean();
	header("HTTP/1.0 403 Access denied");
	global $_BASE_PATH;
	include "$_BASE_PATH/errors/403.php";
	exit;
}
// get the view
function get_view($_view,$_PARAMS=null,$json=false)
{
	global $_BASE_PATH,$_SITE,$_DB;
	global $_PAGE;
	global $_QUERY;
	global $_EP;
	if($json)
	{
		ob_start();
	
	
		if(folder_exists($_BASE_PATH."/sites/$_SITE/ep/$_EP/views/$_view"))
		{
			include $_BASE_PATH."/sites/$_SITE/ep/$_EP/views/$_view/index.php";
		}
		else
		{
			include $_BASE_PATH."/sites/$_SITE/ep/$_EP/views/$_view.php";
		}
		$thebody = ob_get_contents();
		ob_end_clean();
	
		return json_encode(
				Array(
						'body'=>$thebody,
						'title'=>$_TITLE,
						'js'=>$_JS,
						'css'=>$_CSS,
						'meta'=>$_META,
						'jsviews'=>$_SCRIPT_views,
						'jsif'=>$_JSIF,
				)
		);
	}
	else
	{
		ob_start();
	
	
		if(folder_exists("$_BASE_PATH/sites/$_SITE/ep/$_EP/views/$_view"))
		{
			include "$_BASE_PATH/sites/$_SITE/ep/$_EP/views/$_view/index.php";
		}
		else
		{
		include "$_BASE_PATH/sites/$_SITE/ep/$_EP/views/$_view.php";
		}
		$thebody = ob_get_contents();
		ob_end_clean();
		return $thebody;
	}
}
?>