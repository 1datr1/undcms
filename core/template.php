<?php 
$_CSS = Array();
$_JS=Array();
$_JSIF=Array();
$_META=Array();
$_SCRIPT_BLOCKS=Array();
$_SCR_READY_HEAD='';
$_SCR_READY_END='';
$_SCRIPT_BLOCKS_READY=Array();

$_TITLE='';
$_THEME='default';
$_REGIONS=Array();
$_HEAD_READY=false;
// add the css to head
function addcss($_css)
{
	// return if accepted
	global $_HEAD_READY;
	global $_BASE_PATH;
	if($_HEAD_READY) return;
		
	global $_CSS;
	if(!in_array($_css, $_CSS))
		$_CSS[]=$_css;
}
// get the blockmap for current page and theme and e.p.
function get_block_map($route=NULL,$theme=null,$ep=null)
{
	global $_EP,$_SITE,$_THEME;
	global $_BLOCKMAP;
	global $_PAGE_ROUTE;
	global $_BASE_PATH;
	if($theme==null) $theme=$_THEME;
	if($ep==null) $ep=$_EP;
	if($route==null) $route=$_PAGE_ROUTE;
	//echo ">>";
	//var_dump($_BLOCKMAP[$theme][$thepage]);
	include "$_BASE_PATH/sites/$_SITE/ep/$ep/blockmap.php";
	//var_dump($_BLOCKMAP);
	//echo ":: $_BASE_PATH/sites/$_SITE/ep/$_EP/blockmap.php";
	
//echo ">>$thepage";
	$_route=$route;
	$thepage=implode('/', $_route);
	if(!empty($_BLOCKMAP[$theme][$thepage]))
		return $_BLOCKMAP[$theme][$thepage];
	if(!empty($_BLOCKMAP[$theme]["$thepage/*"]))
		return $_BLOCKMAP[$theme]["$thepage/*"];
	for($i=count($route);$i>=0;$i--)
	{
		
		$_route[count($_route)-1]='*';
		$thepage=implode('/', $_route);
		
		if(!empty($_BLOCKMAP[$theme][$thepage]))
			return $_BLOCKMAP[$theme][$thepage];	
		
		unset($_route[count($_route)-1]);
	}
	
}
// push the block $block into the region $reg
function push_block($reg,$block)
{
	global $_BASE_PATH;
	global $_REGIONS;
	if(empty($_REGIONS[$reg]))
		$_REGIONS[$reg][]=$block;
}
// draw the region
function draw_region($reg,$begin='',$end='',$splitter='')
{
	global $_BASE_PATH;
	global $_REGIONS;
	//var_dump($_REGIONS);
	if(empty($_REGIONS[$reg]))
		return '';
	$blocks=Array();
	$i=0;
	foreach ($_REGIONS[$reg] as $idx=>$blck)
	{
		if($i)
			echo $splitter;
		echo $begin;
		echo get_block($blck);
		echo $end;
		$i++;
	}
}


// add the javascript to head
function addjs($_js,$IF=NULL)
{
	global $_BASE_PATH;
	// return if accepted
	if($IF==null)
	{		
		global $_JS;
		if(!in_array($_js, $_JS))
			$_JS[]=$_js;
	}
	else 
	{
		global $_JSIF;
		if(empty($_JSIF[$IF]))
			$_JSIF[$IF]=Array();
		if(!in_array($_js, $_JSIF[$IF]))
			$_JSIF[$IF][]=$_js;
	}
}


function addjsready($_js)
{
	global $_SCRIPT_BLOCKS_READY;
	// return if accepted
	$_SCRIPT_BLOCKS_READY[]=$_js;
	
}

// add the meta keyword
function addmeta($key,$val)
{
	// return if accepted
	global $_HEAD_READY;
	global $_BASE_PATH;
	if($_HEAD_READY) return;
	
	global $_META;
	$_META[$key]=$val;
}
// set title
function title($t)
{
	// return if accepted
	global $_HEAD_READY;
	global $_BASE_PATH;
	if($_HEAD_READY) return;
	
	global $_TITLE;
	$_TITLE=$t;
}
// accept head data
function accept_head()
{
	global $_HEAD_READY;
	global $_BASE_PATH;
	$_HEAD_READY =true;
}
// add script block
function addscript_block($sb,$comment=NULL)
{
	global $_SCRIPT_BLOCKS;
	global $_BASE_PATH;
	if(is_string($comment)) 
		$_SCRIPT_BLOCKS[$comment]=$sb;
	else 
		$_SCRIPT_BLOCKS[]=$sb;
}
?>